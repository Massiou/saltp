package projet;

import java.awt.List;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

public class Annuaire {
	
	/* List of known directories by the current directory */
	public ArrayList<Annuaire> annuaires ;
	/* Name of the file managed by the current directory */
	private String fileName ;
	/* Local adress for the current directory */
	private InetAddress annuaireAddress ;
	/* Local port used by the directory */
	private int port ;
	/* List of fragments associated to the different owners on network */
	public ArrayList<FileFragment> listFragment ;
	
	/**
	 * Constructor #1
	 */
	public Annuaire(int port) 
	{
		this.annuaires        = new ArrayList<Annuaire>() ;
		this.fileName         = null ;
		this.annuaireAddress  = null ;
		this.listFragment	  = null ; 
		this.port = port ;
		
		System.out.println("New directory ("+ getFileName() + ") started on port " + getPort()) ;
	}
	
	/**
	 * Constructor #2 : the initial directory instantiated
	 * @param filename
	 */
	public Annuaire(String filename, int port)
	{
		this.annuaires = new ArrayList<Annuaire>() ;
		this.fileName = filename ;
		this.annuaireAddress = this.getAnnuaireAddress();
		this.port = port ;
		
		System.out.println("New directory ("+ getFileName() + ") started on port " + getPort()) ;
	}
	
	/**
	 * Constructor #3
	 * @param directories : the current list of directories
	 * @param fileName : the new file to manage
	 * @param annuaireAddress : the address of the new directory
	 */
	public Annuaire(ArrayList<Annuaire> currentDirectories, String newFileName, int port, InetAddress newDirectoryAddress) 
	{

		this.annuaires = currentDirectories ;
		this.fileName = newFileName;
		this.annuaireAddress = newDirectoryAddress ;
		this.listFragment = new ArrayList<>() ;
		this.port = port ;
		
		System.out.println("New directory ("+ getFileName() + ") started on port " + getPort()) ;
		
	}

	/* Class Methods */
	
	/**
	 * Check if the 'filename' file is managed by a directory (including himself) on the network
	 * @param fileName
	 * @return
	 */
	public boolean checkFileExistenceOnNetwork(String fileName)
	{
		/* If first directory interrogated manages the file... */
		if(fileName.equals(this.fileName)) {
			return true;
		}
		
		Iterator it = this.getAnnuaires().iterator() ;
		
		while(it.hasNext()) {
			Annuaire annu = (Annuaire) it.next() ;
			
			if(annu.getFileName().equals(fileName)) {
				System.out.println("File found in the directory  @ " + " " + annu.getAnnuaireAddress());
				return true ;
			}
		}
		System.out.println("No directory managing the file " + fileName + ", create a new dir to add it");
		return false ;
	}
	
	/**
	 * Return the directory managing the 'filename' file
	 * @param filename
	 * @return
	 */
	public Annuaire getFileDirectory(String filename)
	{
		/* If first directory interrogated manages the file... */
		if(this.fileName.equals(filename)) {
			return this;
		}
		
		Iterator<Annuaire> iterateOverDirectories = this.annuaires.iterator();
		
		while(iterateOverDirectories.hasNext()){
			Annuaire scannedDirectory = (Annuaire)iterateOverDirectories.next();
			
			/* Correct directory for the 'filename' file has been found...return it!  */
			if(scannedDirectory.getFileName().equals(filename)) {
				System.out.println("Directory found, return it now");
				return scannedDirectory;
			}
		}
		/* No matching directory found, so return null (normally never) */
		return null;
	}
	
	/**
	 * Update all directories directoryList from the local directoryList
	 * @param directoryToInsert
	 */
	public void updateDirectories(Annuaire directoryToInsert)
	{
		if(!this.getAnnuaires().isEmpty()) {
			Iterator iterateOverDirectories = this.getAnnuaires().iterator() ;
			
			while(iterateOverDirectories.hasNext()) {
				
				Annuaire directoryToUpdate = (Annuaire) iterateOverDirectories.next();
				directoryToUpdate.getAnnuaires().add(directoryToInsert);
			}
		}
	}
	
	
	/**
	 * Update the list of directories in all current directories
	 */
	public void majAnnuaire()
	{
		/* Other directories exist on network ? */
		if(!this.getAnnuaires().isEmpty()) {
			
			Iterator it = this.getAnnuaires().iterator() ;
			
			/* Run over all existing directories and update their lists*/
			while(it.hasNext()) {
				/* Directory to update */
				Annuaire annu = (Annuaire) it.next() ;
				/* Update the list from scratch */
				annu.setAnnuaires(new ArrayList<Annuaire>());
				
				Iterator itToCopy = this.getAnnuaires().iterator() ;
				
				/* Run trough the local list of directories */
				while(itToCopy.hasNext()) {
					
					Annuaire annuToCopy = (Annuaire) itToCopy.next();
					/* Check if directory to insert is not himself */
					if(!annu.getFileName().equals( annuToCopy.getFileName() )) {
						annu.getAnnuaires().add(annuToCopy);
					}
				}
				/* Add the current directory to the updated directory too */
				annu.getAnnuaires().add(this) ;
			}
		}
	}
	
	/** 
	 * Get a complete list of fragment composing a file associated with only one owner
	 * @return
	 */
	public ArrayList<FileFragment> getListDownload()
	{
		/* List to send back to the peer */
		ArrayList<FileFragment> allFragments = new ArrayList<FileFragment>();
		
		Iterator iterOverFragments = listFragment.iterator();
		
		while(iterOverFragments.hasNext()) {
			FileFragment currentFrag = (FileFragment) iterOverFragments.next();
			
			/* If fragment of file not added yet...join to the rest */
			/* comparaison based on 'file part' id */
			if(!allFragments.contains(currentFrag)) {
				allFragments.add(currentFrag);
			}
		}
		
		return allFragments;
		
	}
	
	/**
	 * Delete all refs of the 'ia' peer in the local fragments list
	 * @param ia
	 */
	public void deletePeerFromDirectory(InetAddress ia)
	{
		Iterator iterateOverFragments = this.getListFragment().iterator();
		
		while(iterateOverFragments.hasNext()) {
			FileFragment currentFF = (FileFragment) iterateOverFragments.next();
			
			/* If this fragment belongs to the searched owner : remove it from list */
			if(currentFF.getFileFragmentOwner().equals(ia)) {
				iterateOverFragments.remove();
			}
		}
		System.out.println("All fragments managed by " + ia + " successfully removed (" + this.getAnnuaireAddress() + ")");
	}
	
	/**
	 * Delete all refs of the 'ia' peer on network
	 */
	public void deletePairFromNetwork(InetAddress ia)
	{
		Iterator iterateOverDirectories = this.getAnnuaires().iterator();
		
		while(iterateOverDirectories.hasNext()) {
			Annuaire remoteDirectory = (Annuaire) iterateOverDirectories.next();
			/* Remove all ref of the peer on each remote directory */
			remoteDirectory.deletePeerFromDirectory(ia);	
		}
		System.out.println("All fragments managed by " + ia + " successfully removed from network");
	}
	
	/**
	 * Delete the directory designed by the 'filename' file managed
	 * Removed from the directory list
	 * @param fileNameManaged
	 */
	public void deleteDirectory(String fileNameManaged)
	{
		Iterator iterateOverDirectories = this.getAnnuaires().iterator();
		
		while(iterateOverDirectories.hasNext()) {
			Annuaire currentDirectory = (Annuaire) iterateOverDirectories.next();
			
			/* Remove directory if founded */
			if(currentDirectory.getFileName().equals(fileNameManaged)) {
				iterateOverDirectories.remove();
				break;
			}
			System.out.println("Directory successfully removed (" + this.getAnnuaireAddress() + ")");
		}
	}
	
	/**
	 * Remove all reference on network of the directoy designed by the 'filename' file
	 * @param filenameManaged
	 */
	public void deleteAnnuaireFromNetwork(String fileNameManaged)
	{
		Iterator iterateOverDirectories = this.getAnnuaires().iterator();
		
		/* Run through the local directory list */
		while(iterateOverDirectories.hasNext()) {
			Annuaire currentDirectory = (Annuaire) iterateOverDirectories.next();
			
			Iterator iterateOverRemoteDirectories = currentDirectory.getAnnuaires().iterator();
			
			/* Run through the remote directory list */
			while(iterateOverRemoteDirectories.hasNext()) {
				Annuaire remoteDirectory = (Annuaire) iterateOverRemoteDirectories.next();
				
				remoteDirectory.deleteDirectory(fileNameManaged);
			}
			
		}
		System.out.println("Directory successfully removed from network");
	}
	
	/**
	 * Reference a new owner for the file 
	 * @param ia
	 * @param file
	 * @param taillePaquet
	 */
	public void addFragmentReferenceInDirectory(InetAddress ia, File file, int taillePaquet)
	{
		//Le directory recoit un fichier ???????? 
	}
	
	/**
	 * Reference the owner for the 
	 * @param ia
	 * @param file
	 * @param taillePaquet
	 */
	public void addFragmentOwner(InetAddress ia, File file, int taillePaquet)
	{
		if(!this.checkOwnerExistence(ia)){
			//We can add the owner in the directory
			long fileSize = file.length() ;
			long nbLoop = fileSize / taillePaquet ;
			
			System.out.println("file size : " + fileSize);
			System.out.println("nb loop   :" + nbLoop);
			
			int compteur = 0 ;
			int pointeur_debut = 0 ;
			int pointeur_fin = 0 ;
			
	    	while(compteur < nbLoop-1){
	    		pointeur_debut = pointeur_fin+1 ;
	    		pointeur_fin = pointeur_fin+1 + taillePaquet ;
	    		
	    		this.getListFragment().add(new FileFragment(file.getName(), compteur, ia, pointeur_debut, pointeur_fin)) ;
	    		compteur++ ;
	    	}
	    	
	    	//The last part
	    	pointeur_debut = pointeur_fin+1 ;
	    	pointeur_fin = (int) (file.length()%taillePaquet) ;
	    	this.getListFragment().add(new FileFragment(file.getName(), compteur, ia, pointeur_debut, pointeur_fin)) ;
	    	compteur++ ;
	    	
	    	System.out.println(compteur);
		}
	}
	
	/**
	 * 
	 * @param ia
	 * @return
	 */
	public boolean checkOwnerExistenceOnLocalDirectory(InetAddress ia){
		String ownerIp = ia.getHostAddress() ;
		
		Iterator it = this.getListFragment().iterator() ;
		
		while(it.hasNext()) {
			
			FileFragment ff = (FileFragment) it.next() ;
			String ffIp = ff.getFileFragmentOwner().getHostAddress() ;
			
			if(ffIp.equals(ownerIp)){
				System.out.println("Owner already in the directory");
				return true ;
			}
		}
		System.out.println("Owner can be added in the directory");
		return false ;
	}
	
	/**
	 * Check on network if the 'fileName' file is managed
	 * @param fileName
	 * @return the directory managing the 'fileName' file
	 */
	public Annuaire checkDirectoriesOnNetwork(String fileName)
	{
		Iterator iterateOverDirectories = this.getAnnuaires().iterator() ;
		
		while(iterateOverDirectories.hasNext()) {
			Annuaire remoteDirectory = (Annuaire) iterateOverDirectories.next() ;
			
			if(remoteDirectory.fileName.equals(fileName)) {
				System.out.println("Directory " + remoteDirectory.getAnnuaireAddress() + " found for file " + fileName);
				return remoteDirectory ;
			}
		}
		System.out.println("No directory found for file " + fileName + ". \n Stop searching");
		return null ;
	}
	
	
	public ArrayList<Annuaire> getAnnuaires() 
	{
		return this.annuaires ;
	}
	public void setAnnuaires(ArrayList<Annuaire> annuaires) 
	{
		this.annuaires = annuaires ;
	}
	public String getFileName() 
	{
		return this.fileName ;
	}
	public void setFileName(String fileName) 
	{
		this.fileName = fileName ;
	}
	public InetAddress getAnnuaireAddress() 
	{
		return this.annuaireAddress ;
	}
	public int getPort()
	{
		return this.port ;
	}
	public void setPort(int port)
	{
		this.port = port ;
	}
	
	public void setAnnuaireAddress(InetAddress annuaireAddress) {
		this.annuaireAddress = annuaireAddress;
	}
	public ArrayList<FileFragment> getListFragment() {
		return listFragment;
	}
	public void setListFragment(ArrayList<FileFragment> listFragment) {
		this.listFragment = listFragment;
	}
	
	
	/**
	 * Main function for directories
	 * @param args
	 * @throws IOException
	 */
	public static void main(String...args) throws IOException
	{	
		/* Local adress for the current directory */
		InetAddress annuaireAddress = InetAddress.getLocalHost() ;
		/* Is demon server running ? */
		boolean demon = true ; 
		
		/* data definition */
		byte[] clientData = new byte[100] ;
        byte[] serverData = new byte[100] ;
		
        int port = 18000 ;
		
        
        
        ServerSocket  servSock = new ServerSocket(port, 5) ;
		
		/**
		 * waiting for connection
		 */
		
		while(demon){
			
			/*  */
			Socket socketServer  = servSock.accept();
			InputStream inputData = socketServer.getInputStream();
			inputData.read(clientData);
			
			socketServer.
			
			boolean _knownClient = true;
			_knownClient  = scanIPTable();
			
			if(_knownClient) {
				
			}
			
			
			
			
			
			
			
			
			
		}
		
		/* Fonctions for main run */
		
		
		
		
		
		
		
		
		/*
		
		
		File file = new File("F:\\image_client.jpg") ;
	
		Annuaire annuaire = new Annuaire(annuaires, file.getName(), annuaireAddress) ;
		
		annuaire.checkFileExistence(file.getName()) ;
		
		annuaire.addFragmentOwner(InetAddress.getLocalHost(), file, 1000) ;
		annuaire.addFragmentOwner(InetAddress.getLocalHost(), file, 1000) ;
		*/
	}
	
	/* Fonctions for main run */
	public static String displayMenu(){
    	String s = "" + 
    			"-==========================================================-\n" +
    			"-                                                          -\n" +
    			"-                         SALTP DIRECTORY                           -\n" +
    			"-                                                          -\n" +
    			"-==========================================================-\n" +
    			"ACTIONS :\n" +
    			"1 - Run demon \n" +
    			"2 - Kill demon \n" +
    			"3 - Exit\n" +
    			"8 - TEST getConfig\n" +
    			"9 - TEST\n"
    				;
    	return s ;
    }
}
	
