package projet;

import java.util.Date;

public class AnnuaireMessage {

	/* Type of the message */
	private String type;
	/* Operation code of the message */
	private int ope;
	/* Receipt timestamp of the message */
	private Date timestamp;
	/* Data associated to the message */
	private String data;
	
	/** 
	 * Constructor for messages
	 * @param typeCode
	 * @param opeCode
	 */
	public AnnuaireMessage(String typeCode, int opeCode){
		type = typeCode;
		ope = opeCode;
		timestamp = new Date();
	}
	
	/**
	 * Compact parameters of the message into a single 'String' message
	 * @param messageToCompact
	 * @return all message parameters packed
	 */
	public String compact(){
		return this.getType() + " " + this.getOpe() + " " + this.getData();
	}
	
	
	public String getType(){
		return type;
	}
	
	public int getOpe(){
		return ope;
	}
	
	public Date getTimestamp(){
		return timestamp;
	}
	
	public String getData(){
		return data;
	}
	
	public void setType(String typeToSet){
		type = typeToSet;
	}
	
	public void setOpe(int opeToSet){
		ope = opeToSet;
	}
	
	public void setData(String dataToSet){
		data = dataToSet;
	}
	
}
