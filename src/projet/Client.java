package projet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Client{
	static int taillePaquet = 1000;
	
    public static void main(String...args) throws IOException { 
    	String promptInput = "0" ;
    	
    	//Initialisation of the directory
    	Annuaire annu = new Annuaire() ;
    	
    	//Display the menu while the user doesn't exit byt the [3] command
    	while(promptInput != "3"){
	    	System.out.println(displayMenu());
	    	
	    	//Input of the command
	    	Scanner scanner = new Scanner( System.in );
	    	promptInput = scanner.nextLine() ;
	    	System.out.println("choix n�" + promptInput);
    	
  
	    	switch(promptInput){
	    		case "1":     	
	    			//choix du fichier
	    			System.out.println("Veuillez indiquer l'emplacement de votre fichier");
	    
	    	        JFileChooser chooser = new JFileChooser();
	    	        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif", "txt");
	    	        chooser.setFileFilter(filter);
	    	        int returnVal = chooser.showOpenDialog(null);
	    	        if(returnVal == JFileChooser.APPROVE_OPTION) {        
	    	           
	    	        	String fullPathFile = chooser.getSelectedFile().getCanonicalPath() ;
	    	        	String fileName = chooser.getSelectedFile().getName() ;
	    	        	
	    	        	System.out.println("Information sur le fichier :" +
	    	        			"Full_path : " + fullPathFile + "\n" +
	    	        			"Filename  : " + fileName
	    	        	);
	    	        	
	    	        	//Check of the existence of this file in a directory and potential instantiation
	    	        	if(!annu.checkFileExistence(fileName)){
	    	        		//Instantiation of a new directory
	    	        		System.out.println("CLIENT : L'annuaire associ� � " + fileName + " n'h�xiste pas");
	    	        		Annuaire new_annu = new Annuaire(fileName) ;
	    	        		
	    	        		//UPD of the directories
	    	        		annu.getAnnuaires().add(new_annu) ;
	    	        		
	    	        		Iterator it = annu.getAnnuaires().iterator() ;
	    	        		while(it.hasNext()){
	    	        			Annuaire annuCheck = (Annuaire) it.next() ;
	    	        			annuCheck.majAnnuaire() ;
	    	        			System.out.println("UPD of " + annuCheck.getFileName() + " done");
	    	        		}
	    	        		
	    	        	}else{
	    	        		//Directory already exist
	    	        		System.out.println("The directory already exist !");
	    	        		
	    	        		//ADD this user to the directories if it doesn't exist already
	    	        	}
	    	        	
	    	        	
	    	        	
	    	        }
	    	        else{
	    	        	//annulation de la proc�dure
	    	        }   
	    			
	    			break ;
	    		case "2":     			
	    			break ;
	    		case "3":     
	    			System.out.println("CLIENT : EXIT");
	    			break ;
	    		case "4":     			
	    			break ;
	    		case "8":
	    			System.out.println("CLIENT : Config :");
	    			System.out.println("HOST : " + InetAddress.getLocalHost());
	    			System.out.println("IP   : " + InetAddress.getLocalHost().getHostAddress());
	    			
	    			break ;
	    		case "9":     
	    			test() ;
	    			break ;
	    		default:
	    			System.out.println("Invalid instruction");
	    			break ;    			
	    	}
    	}
    	System.out.println("CLIENT : EXIT !");
    } 
    
    public static String displayMenu(){
    	String s = "" + 
    			"-==========================================================-\n" +
    			"-                                                          -\n" +
    			"-                         SALTP                            -\n" +
    			"-                                                          -\n" +
    			"-==========================================================-\n" +
    			"Veuillez entrez un num�ro parmi les choix suivants :\n" +
    			"1 - Ajouter un fichier \n" +
    			"2 - Rechercher un fichier \n" +
    			"3 - Quitter\n" +
    			"8 - TEST getConfig\n" +
    			"9 - TEST\n"
    				;
    	return s ;
    }
    
    public static void test() throws UnknownHostException, IOException{
        Socket sock = new Socket(InetAddress.getLocalHost(),9001);
        
        System.out.println("Choix du fichier");
        //choix du fichier
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JPG & GIF Images", "jpg", "gif", "txt");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {        
           
        	System.out.println("" +
        			"Full_path : " + chooser.getSelectedFile().getCanonicalPath() + "\n" +
        			"Filename  : " + chooser.getSelectedFile().getName()
        	);
           
        	int dot = chooser.getSelectedFile().getCanonicalPath().lastIndexOf('.') +1 ;
        	System.out.println("Extension : " + chooser.getSelectedFile().getCanonicalPath().substring(dot) );
           
//         transfertFile(
//        		 new FileInputStream(chooser.getSelectedFile().getCanonicalFile()),
//        		 sock.getOutputStream(),
//        		 true);
        	
        	transferFragmentedFile(
        			chooser.getSelectedFile().getCanonicalPath(), 
        			new FileInputStream(chooser.getSelectedFile().getCanonicalFile()), 
        			sock.getOutputStream(), 
        			true) ;
         
        }
        else{
        	//annulation de la proc�dure
        }      
        sock.close();
    }
    
    public static void sendMessage(Socket s, String message) throws IOException {
        s.getOutputStream().write(message.getBytes("UTF-8"));
        s.getOutputStream().flush();
    }
    
    public static void transfertFile(InputStream in, OutputStream out, boolean closeOnExit) throws IOException{
        byte buf[] = new byte[1024];
        
        int n;
        while((n=in.read(buf))!=-1)
            out.write(buf,0,n);
        
        if (closeOnExit){
            in.close();
            out.close();
        }
    }
    
    /**
     * 
     * Transfer d'un fichier par morceau d�fini en param�tre
     */
    public static void transferFragmentedFile(String pathName, InputStream inS, OutputStream out, boolean closeOnExit) throws IOException{
    	
    	File f = new File(pathName) ;
    	System.out.println("Envoi du fichier " + f.getName());
    	
    	long tailleFichier = f.length() ;
    	System.out.println("taille du fichier : " + tailleFichier);
    	
    	long nbpassageSuppose = tailleFichier/taillePaquet ;
    	
    	System.out.println("Nombre de passage suppos�s : " + nbpassageSuppose);
    	
    	InputStream in = new BufferedInputStream(inS) ;
    	ByteArrayOutputStream tableauBytes = new ByteArrayOutputStream() ;
    	BufferedOutputStream tampon = new BufferedOutputStream(tableauBytes) ;
    	
    	int lu = in.read() ;
    	int[] aEcrire = new int[taillePaquet] ;
    	int compteur = 0 ;
    	long ouOnEstRendu = 0 ;
    	
    	//Tant que l'on est pas � la fin du fichier
    	while(lu > -1){
    		//On lit les donn�es du fichier
    		aEcrire[compteur] = lu ;
    		lu = in.read();
    		compteur++ ;
    		
    		//Quand on rempli le tableau, on envoie un paquet de [taillePaquet] octets
    		if(compteur == taillePaquet){
    			compteur = 0 ;
    			ouOnEstRendu++ ;
    			
    			//On rempli le tampon
    			for(int x=0 ; x<taillePaquet ; x++){
    				tampon.write(aEcrire[x]) ;
    			}
    			
    			//Et on l'envoi
    			out.write(tableauBytes.toByteArray()) ;
    			tableauBytes.reset() ;
    			System.out.println("Avancement : " + (float) ouOnEstRendu/nbpassageSuppose * 100 + "%");
    		}
    	}
    	
    	//On envoie le dernier paquet, qui ne fait pas forc�ment [taillePaquet]
    	for(int x=0 ; x<compteur ; x++){
    		tampon.write(aEcrire[x]) ;
    	}	
		
    	//Et on l'envoie
		tampon.flush() ;
		out.write(tableauBytes.toByteArray()) ;
		out.flush() ;
		
		System.out.println("Avancement: "+(float) ouOnEstRendu/nbpassageSuppose * 100+"%");
		
		System.out.println("fini");
		in.close() ;
		tampon.close() ;
		System.out.println("Passage effectues : " + ouOnEstRendu);
    	
    }
    
    
}