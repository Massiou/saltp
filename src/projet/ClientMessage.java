package projet;

import java.util.Date;

/**
 * 
 * @author Matthieu LACOMBE and Jean-Louis DUONG
 * 
 *
 */
public class ClientMessage {

	public ClientMessage(String typeCode, int opeCode){
		type = typeCode;
		ope = opeCode;
		timestamp = new Date();
	}
	
	/**
	 * Compact parameters of the message into a single String
	 * @param messageToCompact
	 * @return all message parameters packed
	 */
	public String compact(){
		return this.getType() + " " + this.getOpe() + " " + this.getData();
	}
	
	
	public String getType(){
		return type;
	}
	
	public int getOpe(){
		return ope;
	}
	
	public Date getTimestamp(){
		return timestamp;
	}
	
	public String getData(){
		return data;
	}
	
	public void setType(String typeToSet){
		type = typeToSet;
	}
	
	public void setOpe(int opeToSet){
		ope = opeToSet;
	}
	
	public void setData(String dataToSet){
		data = dataToSet;
	}
	
	
	/* type of the message */
	private String type;
	/* operation code of the message */
	private int ope;
	/* receipt timestamp of the message */
	private Date timestamp;
	/* data associated to the message */
	private String data;
}
