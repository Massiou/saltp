package projet;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ClientSALTP {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		
		ClientSALTP client = new 
		
		/* auto-declaration of the peer on network */
		initOnNetwork();
		
		/* Render the menu to the user*/
		try {
			System.out.println(displayMenu());
		} catch (UnknownHostException e) {
			System.out.println("Erreur - Erreur � la r�cup�ration des informations h�te");
			e.printStackTrace();
		}
		
		/* Input of the menu */
    	Scanner scanner = new Scanner( System.in );
    	promptInput = scanner.nextLine() ;
    	
    	/*
    	 * Conditional process according to the user choice
    	 */
    	switch(promptInput){
    		/* add file in deposit */
    		/*
    		 * 	- 
    		 * 
    		 */
    		case "1":
    			
    		break;
    		/* look for file on network */
    		case "2":
    			
    		break;
    		/* exit */
    		case "3":
    			
    		break;
    	}

	}
	
	/**
	 * return a new socket connection with the "adrsToCo" remote
	 * @param adrsToCo
	 * @return
	 */
	public Socket initConnect(String adrsToCo){
		
		try {
			Socket socket = new Socket(adrsToCo, 9001);
			return socket;
		} catch (UnknownHostException e) {
			System.out.println("Error - The host specified does not exist");
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			System.out.println("Error - Error while initializing connection");
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * first action for a new peer on network
	 * - scan its own deposit
	 * - inform the rest of the network about what it has in it
	 */
	public static void initOnNetwork(){

		ArrayList<String> myOwnFiles = scanHostDeposit();
		
		Iterator it = myOwnFiles.iterator();
		
		while(it.hasNext()){
			String currentFile = (String) it.next(); 
			ClientMessage whatIHave = new ClientMessage("REF", 1);
			whatIHave.setData(currentFile);
			
			
		}
		
		
		
	}
	
	
	/**
	 * @return the menu rendered to the user
	 * @throws UnknownHostException 
	 */
	public static String displayMenu() throws UnknownHostException{
    	String s = "" + 
    			"-==========================================================-\n" +
    			"-                                                          -\n" +
    			"-                         SALTP                            -\n" +
    			"-                                                          -\n" +
    			"-==========================================================-\n" +
    			"Votre configuration : \n" +
    			"\t HOST : " + InetAddress.getLocalHost() + "\n" +
    			"\t ADRESS : " + InetAddress.getLocalHost().getHostAddress() + "\n" +
    			"Veuillez entrez un num�ro parmi les choix suivants :\n" +
    			"1 - Ajouter un fichier depuis votre d�pot \n" +
    			"2 - Rechercher un fichier sur le r�seau \n" +
    			"3 - Quitter\n";
    	return s ;
    }
	
	/**
	 * Convert a ClientMessage into a sendable serie of bytes and post it
	 * @param messageToSend
	 * @param remoteSocket
	 */
	public void sendMessage(ClientMessage messageToSend, Socket remoteSocket){
		
		String compactMessage = messageToSend.compact();
		
		try {
			remoteSocket.getOutputStream().write(compactMessage.getBytes("UTF-8"));
			
		} catch (UnsupportedEncodingException e) {
			System.out.println("Erreur - Erreur lors de l'encodage du message " + messageToSend.getType() + " (OpeCode " + messageToSend.getOpe() + ")");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Erreur - Erreur d'envoi du message " + messageToSend.getType() + " (OpeCode " + messageToSend.getOpe() + ")");
			e.printStackTrace();
		}
		try {
			remoteSocket.getOutputStream().flush();
			System.out.println("Message successful delivered to " + remoteSocket.getInetAddress().getHostAddress());
		} catch (IOException e) {
			System.out.println("Erreur - Message incomplet " + messageToSend.getType() + " (OpeCode " + messageToSend.getOpe() + ")");
			e.printStackTrace();
		}
	}
	
	/**
	 * scan and get a list of marks present in host deposit
	 * @return a String list of marks
	 */
	public static ArrayList<String> scanHostDeposit(){
		ArrayList<String> results = new ArrayList<String>();
		File[] files = new File(DEPOSITPATH).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		    	// TODO : int�grer le checksum MD5 � cet endroit ??? Je ne sais gu�re...
		        results.add(file.getName());
		    }
		}
		return results;
	}
	
	
	
	/* List of known directories by the peers */
	public ArrayList<String> directoryList = new ArrayList<String>();
	/* The only address known by peer to start */
	public String initialAdress ;
	/* Choice of the user into the menu */
	public static String promptInput = "0" ;
	/* Fixed path of the host deposit */
	public final static String DEPOSITPATH = "/Path/Of/Deposit/Here";
}
