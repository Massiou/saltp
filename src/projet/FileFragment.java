package projet;

import java.net.InetAddress;

public class FileFragment {
	
	/* Initial file name of the fragment */
	public String fileName ; 
	/* Which part of the total file is it ? */
	public int filePart ;
	/* Adress of the owner */
	public InetAddress fileFragmentOwner ;
	/* Id of the first part of the file */
	public int filePartBegin ;
	/* Id of the last part of the file */
	public int filePartEnd ;
	
	/**
	 * Constructor of a file fragment
	 * @param fileName
	 * @param filePart
	 * @param fileFragmentAdress
	 * @param filePartBegin
	 * @param filePartEnd
	 */
	public FileFragment(String fileName, int filePart, InetAddress fileFragmentAdress, int filePartBegin, int filePartEnd) {
		this.fileName = fileName;
		this.filePart = filePart;
		this.fileFragmentOwner = fileFragmentAdress;
		this.filePartBegin = filePartBegin;
		this.filePartEnd = filePartEnd;
		
		System.out.println(this.toString()); ;
	}
	
	
	public String toString() {
		return "FileFragment [fileName=" + fileName + ", filePart=" + filePart
				+ ", fileFragmentOwner=" + fileFragmentOwner
				+ ", filePartBegin=" + filePartBegin + ", filePartEnd="
				+ filePartEnd + "]";
	}

	/**
	 * Reimplemente equals methode
	 * @param fragToCompare
	 * @return
	 */
	public boolean equals(FileFragment fragToCompare) 
	{
		if(this.filePart == fragToCompare.getFilePart()) {
			return true;
		}else {
			return false;
		}
	}


	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getFilePart() {
		return filePart;
	}
	public void setFilePart(int filePart) {
		this.filePart = filePart;
	}
	public InetAddress getFileFragmentOwner() {
		return fileFragmentOwner;
	}
	public void setFileFragmentOwner(InetAddress fileFragmentOwner) {
		this.fileFragmentOwner = fileFragmentOwner;
	}
	public int getFilePartBegin() {
		return filePartBegin;
	}
	public void setFilePartBegin(int filePartBegin) {
		this.filePartBegin = filePartBegin;
	}
	public int getFilePartEnd() {
		return filePartEnd;
	}
	public void setFilePartEnd(int filePartEnd) {
		this.filePartEnd = filePartEnd;
	}
	
}
