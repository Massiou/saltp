package projet;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur
{
    public static void main(String...args) throws IOException 
    { 
    	System.out.println("SERVEUR : Cr�ation du serveur");
       
    	Socket sock = new ServerSocket(9001).accept();
        System.out.println("SERVEUR : Connexion accept�e");
        
//        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(sock.getInputStream())) ;        
//        System.out.println("SERVEUR : Message re�u : " + inFromClient.readLine());
        
        transfert(
                sock.getInputStream(),
                new FileOutputStream("F:\\image_serveur.jpg"),
                true);
        
        System.out.println("fichier transf�r�e");
        sock.close(); 
    } 
    
    public static void transfert(InputStream in, OutputStream out, boolean closeOnExit) throws IOException
    {
        byte buf[] = new byte[1024];
        
        int n;
        while((n=in.read(buf))!=-1)
            out.write(buf,0,n);
        
        if (closeOnExit)
        {
            in.close();
            out.close();
        }
    }
}